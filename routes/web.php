<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/app', function () { return view('home'); });

// user
Route::get('/editar-perfil', 'UsuarioController@listar_informacoes_perfil');
Route::post('/atualizar-perfil', 'UsuarioController@editar_perfil');
Route::post('/efetuar-login', 'UsuarioController@efetuar_login');

// mochilas
Route::get('/cadastrar-mochila', function () { return view('cadastrar_mochila'); });
Route::get('/ver-mochilas', 'MochilaController@listar_mochilas');
Route::post('/cadastro-mochila', 'MochilaController@cadastrar_mochila');
