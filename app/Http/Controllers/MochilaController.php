<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mochila;
use Session;

class MochilaController extends Controller
{
	function listar_mochilas() {

		$mochilas = Mochila::orderBy('id', 'desc')->get();

		return view('ver_mochilas', [
			"array_mochilas" => $mochilas
		]);
	}

	function cadastrar_mochila(Request $request) {

		$query = Mochila::create([
			'titulo' => $request->titulo,
			'publica' => $request->publica,
			'ativa' => $request->ativa,
			'id_autor' => session('id'),
		]);

		if($query) {
			return 1;
		} else {
			return "Ocorreu um erro durante esta requisição.";
		}

	}

	function pesquisa_mochilas(Request $request) {

	}
}
