<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use Session;

class UsuarioController extends Controller
{
    function efetuar_login(Request $request) {

      $usuario = Usuario::where([
        'username'  => $request->username,
        'senha'     => base64_encode($request->senha),
      ])->first();

      if(sizeof($usuario) > 0) {

        session([

          "id"        => $usuario->id,
          "nome"      => $usuario->nome,
          "username"  => $usuario->username,
          "animation" => 1,

        ]);

        return 1;

      } else {

        return "Usuário e/ou senha não são válidos.";

      }

    }

    function editar_perfil(Request $request) {

      $user = Usuario::where('id', session('id'))->first();

      $nome   = $request->nome;
      $senha  = base64_encode($request->senha);

      if($user->nome != $nome || $user->senha != $senha)
      {
        $query = Usuario::where('id', session('id'))->update([
          'nome'  => $nome,
          'senha' => $senha,
        ]);

        if($query) {

          session([

          "nome"      => $nome,

        ]);

          return 1;
        }
        else
          return "Ocorreu um erro durante esta requisição.";
      }
      else
      {
        return 1;
      }


    }

    function listar_informacoes_perfil() {

      $usuario = Usuario::where('id', session('id'))->first();

      return view('perfil', ['user' => $usuario]);

    }
}
