<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mochila extends Model
{
    protected $table = "mochilas";

    protected $fillable = [
      'titulo',
      'publica',
      'ativa',
      'id_autor'
    ];
}
