var form_perfil = new Vue({
  el: "#form_perfil",
  methods: {
    post_perfil: function() {
      var validacao = false;

      $("#form_perfil").find('input').each(function(i, v){
        if($(v).val() == ''){
          validacao = true;
          $(v).addClass("error");
        }
      });

      if(validacao == true)
      {
        swal_functions.swal_padrao('Campos vazios!', 'Por favor, preencha os campos com borda vermelha.', 'error', "Ok! Vou verificar.", false);
      }
      else {
        if($("#input_senha").val() == $("#input_confirmar_senha").val()) {

          var dados = new FormData();

          dados.append("nome", $("#input_nome").val());
          dados.append("senha", $("#input_senha").val());

          axios.post("/atualizar-perfil", dados).then(function(response) {

            if(response.data == 1) {

              swal_functions.swal_padrao('Tudo certo!', 'Perfil atualizado com sucesso.', 'success', "Continuar", false);

              $("#form_perfil input").removeClass("error");
              $("#input_confirmar_senha").val("");

            } else {

              swal_functions.swal_padrao('Erro do sistema!', response.data, 'error', "Ok! Tentarei mais tarde.", false);

            }
          }).catch(function(error) {

            swal_functions.swal_padrao('Erro do sistema!', error.message, 'error', "Ok! Tentarei mais tarde.", false);

          });

        } else {

          swal_functions.swal_padrao('Erro do sistema!', 'As senhas inseridas não são iguais.', 'error', "Ok! Vou verificar.", false);

        }
      }

    },
    limpar_formulario: function() {

      $("#form_perfil input").removeClass("error");
      $("#input_nome").val("");
      $("#input_senha").val("");
      $("#input_confirmar_senha").val("");

    }
  }

});