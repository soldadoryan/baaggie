var form_login = new Vue({
  el: "#form_login",
  data: {
   message: "",
   show: false,
 },
 methods: {
  post_login: function() {
    var dados = new FormData();

    dados.append('username', $("#input_username").val());
    dados.append('senha', $("#input_senha").val());

    axios.post("/efetuar-login", dados).then(function(response) {

      if(response.data == 1) {

        window.location="/app";

      } else {

        form_login.message = response.data;
        if(form_login.show != true) { form_login.show = true; } else {

          $(".painel-erro").fadeOut(250, function() {
            $(this).fadeIn(250);
          });
        }
      }
    }).catch(function(error) {

      this.message = error.message;
      this.show = true;

    });

  }
}
});
