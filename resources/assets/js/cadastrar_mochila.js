var form_cadastrar_mochila = new Vue({
  el: "#form_cadastrar_mochila",
  methods: {
    post_cadastrar_mochila: function() {

      var validacao = false;

      $("#form_cadastrar_mochila").find('input, select').each(function(i, v){
        if($(v).val() == ''){
          validacao = true;
          $(v).addClass("error");
        }
      });

      if(validacao == true)
      {
        swal_functions.swal_padrao('Campos vazios!', 'Por favor, preencha os campos com borda vermelha.', 'error', "Ok! Vou verificar.", false);
      }
      else {
        var dados = new FormData();

        dados.append("titulo", $("#input_titulo").val());
        dados.append("publica", $("#input_publica").val());
        var btn_check = $("#input_ativa").siblings('div').find('a.active');

        if(btn_check.attr('is') == 0) {
          dados.append("ativa", 1);
        } else {
          dados.append("ativa", 0);
        }

        axios.post("/cadastro-mochila", dados).then(function(response) {

          if(response.data == 1) {

            swal_functions.swal_padrao('Tudo certo!', 'Mochila criada com sucesso.', 'success', "Continuar", true, "/ver-mochilas");

          } else {

            swal_functions.swal_padrao('Erro do sistema!', response.data, 'error', "Ok! Tentarei mais tarde.", false);

          }
        }).catch(function(error) {

          swal_functions.swal_padrao('Erro do sistema!', error.message, 'error', "Ok! Tentarei mais tarde.", false);

        });

      }
    },
    limpar_formulario: function() {

      $("#form_cadastrar_mochila input, #form_cadastrar_mochila select").removeClass("error");
      $("#input_nome").val("");
      $("#input_senha").val("");
      $("#input_titulo").val("");
      $("#blank_select").prop('selected', true);
      $("#input_ativa").prop('checked', true);

    }
  }
});


// checkbox js
$(':checkbox').checkboxpicker({
  html: true,
  offLabel: '<span class="glyphicon glyphicon-remove">',
  onLabel: '<span class="glyphicon glyphicon-ok">'
});

