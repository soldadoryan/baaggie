
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 window.Vue = require('vue');
 window.axios = require('axios');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 Vue.component('example-component', require('./components/ExampleComponent.vue'));

 $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
    'Content-Type': false,
    'Proccess-Data': false,
  }
});

 window.swal_functions = {
  swal_padrao: function(titulo, texto, tipo, texto_botao, then, pagina="") {
    if(then == false) {
      swal({
        title: titulo,
        text: texto,
        type: tipo,
        confirmButtonText: texto_botao,
        allowEnterKey: false,
        allowEscapeKey: false,
        allowOutsideClick: false,
      });
    } else {
      swal({
        title: titulo,
        text: texto,
        type: tipo,
        confirmButtonText: texto_botao,
        allowEnterKey: false,
        allowEscapeKey: false,
        allowOutsideClick: false,
      }).then(function () {
        window.location = pagina;
      });

    }
  }
}
