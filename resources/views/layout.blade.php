<!DOCTYPE HTML>
<html>
<head>
  <title>baaggie - {{ session('nome') }}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="" />
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
  <link rel="icon" type="image/png" href="fav.png">
  <link href="/css/animate.css" rel="stylesheet">
  <link href="assets_admin/css/bootstrap.css" rel='stylesheet' type='text/css' />
  <link href="assets_admin/css/style.css" rel='stylesheet' type='text/css' />
  <link href="assets_admin/css/font-awesome.css" rel="stylesheet">

  @yield('css_custom')


  <script src="assets_admin/js/skycons.js"></script>

  <script src="assets_admin/js/jquery-1.11.1.min.js"></script>
  <script src="assets_admin/js/modernizr.custom.js"></script>
  <link href='//fonts.googleapis.com/css?family=Comfortaa:400,700,300' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Muli:400,300,300italic,400italic' rel='stylesheet' type='text/css'>
  <script src="assets_admin/js/metisMenu.min.js"></script>
  <script src="assets_admin/js/custom.js"></script>
  <link href="assets_admin/css/custom.css" rel="stylesheet">
  <link href="assets_admin/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <script src="assets_admin/js/jquery.sparkline.min.js"></script>

</head>
<body class="cbp-spmenu-push">
  <div class="main-content">
    <div class="sidebar" role="navigation">
      <div class="navbar-collapse">
        <nav class="<?php if(session('animation') == 1) { ?>animated slideInRight <?php } ?> cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right dev-page-sidebar mCustomScrollbar _mCS_1 mCS-autoHide mCS_no_scrollbar" id="cbp-spmenu-s1">
          <div class="scrollbar scrollbar1">
            <ul class="nav" id="side-menu">
              <li>
                <a href="/app"><i class="fa fa-home nav_icon"></i>Página Inicial</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-briefcase nav_icon"></i>Mochilas <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li>
                    <a href="/cadastrar-mochila">Cadastrar</a>
                  </li>
                  <li>
                    <a href="ver-mochilas">Ver Mochilas</a>
                  </li>
                </ul>

              </li>
              <li>
                <a href="#"><i class="fa fa-book nav_icon"></i>Matérias <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li>
                    <a href="buttons.html">Cadastrar</a>
                  </li>
                  <li>
                    <a href="typography.html">Ver Todas</a>
                  </li>
                </ul>

              </li>

              <li>
                <a href="#"><i class="fa fa-pencil-square-o nav_icon"></i>Anotações <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li>
                    <a href="buttons.html">Cadastrar</a>
                  </li>
                  <li>
                    <a href="typography.html">Ver Todas</a>
                  </li>
                </ul>

              </li>

              <li>
                <a href="#"><i class="fa fa-users nav_icon"></i>Usuários <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li>
                    <a href="buttons.html">Cadastrar</a>
                  </li>
                  <li>
                    <a href="typography.html">Ver Todos</a>
                  </li>
                </ul>

              </li>
              <li>
                <a href="#"><i class="fa fa-gears nav_icon"></i>Configurações <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li>
                    <a href="/editar-perfil">Meu perfil</a>
                  </li>
                  <li>
                    <a href="typography.html">Sair</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
    <div class="sticky-header header-section <?php if(session('animation') == 1) { ?>animated slideInLeft <?php } ?>">
      <div class="header-left">
        <div class="logo">
          <a href="/app"><img src="/img/logob.png" class="logo-menu" alt=""></a>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="header-right">
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <?= session('nome') ?> <i class="fa fa-caret-down "></i></a>
            <ul class="dropdown-menu anti-dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>Opções de usuário:</h3>
                </div>
              </li>
              <li><a href="/editar-perfil">
               <div class="notification_desc">
                <p>Meu perfil</p>
                <p><span>Atualize suas informações</span></p>
              </div>
              <div class="clearfix"></div>
            </a></li>
            <li class="odd"><a href="/">
              <div class="notification_desc">
                <p>Sair</p>
                <p><span>Encerrar sessão</span></p>
              </div>
              <div class="clearfix"></div>
            </a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <div id="page-wrapper">
    <div class="main-page">

      @yield('content')

    </div>
  </div>

</div>
<script src="assets_admin/js/classie.js"></script>
<script type="text/javascript" src="assets_admin/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.10.3/sweetalert2.all.min.js"></script>
<script src="https://use.fontawesome.com/88f58b9255.js"></script>

@yield('scripts_custom')

<?php
  if(session('animation') == 1) {
    session(['animation' => 0]);
  }
?>
</body>
</html>
