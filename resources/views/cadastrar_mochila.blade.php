@extends('layout')

@section('css_custom')

<link href="/css/app.css" rel="stylesheet">
<link href="/css/home.css" rel="stylesheet">

@endsection

@section('content')



<div class="painel animated animated bounceInDown">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/app">Página Inicial</a></li>
    <li class="breadcrumb-item active">Mochila</li>
    <li class="breadcrumb-item active">Cadastrar Mochila</li>
  </ol>
  <form id="form_cadastrar_mochila" @submit.prevent="post_cadastrar_mochila">
    <div class="row">
      <div class="col-lg-6">
        <label>Título:</label>
        <input type="text" id="input_titulo" class="input-form">
      </div>
      <div class="col-lg-6">
        <label>Autor:</label>
        <input type="text" class="input-form" value="{{ session('nome') }}" disabled>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <label>Mochila Pública?</label>
        <select type="password" id="input_publica" class="input-form">
          <option id="blank_select" value="" selected>-- Selecione uma opção --</option>
          <option value=""></option>
          <option value="P">Pública</option>
          <option value="L">Privada</option>
          <option value="R">Restrita</option>

        </select>
      </div>
      <div class="col-lg-6">
        <div class="check-group">
          <label>Mochila ativa:</label>
          <br>
          <input type="checkbox" id="input_ativa" data-reverse checked data-group-cls="btn-group-md">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-offset-8 col-lg-4">
        <div class="wrap-buttons">
          <button type="submit" class="btn-sucesso">Cadastrar Mochila</button>
          <button type="button" v-on:click="limpar_formulario" class="btn-atencao">Limpar Campos</button>
        </div>
      </div>
    </div>
  </form>
</div>

@endsection

@section('scripts_custom')

<script src="js/app.js"></script>
<script src="js/check.js"></script>
<script src="js/cadastrar_mochila.js"></script>

@endsection