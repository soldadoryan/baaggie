@extends('layout')

@section('css_custom')

<link href="/css/home.css" rel="stylesheet">

@endsection

@section('content')

<div class="four-grids <?php if(session('animation') == 1) {?> animated slideInLeft <?php } else { ?> animated bounceInDown <?php } ?>">
  <div class="col-md-3 four-grid">
    <div class="four-grid1">
      <div class="icon">
        <i class="glyphicon glyphicon-user" aria-hidden="true"></i>
      </div>
      <div class="four-text">
        <h3>Usuário</h3>
        <h4> <?= session('nome') ?> </h4>
      </div>
      <a href="#" data-toggle="modal" data-target="#myModal1">Ver Mais</a>
    </div>
  </div>
  <div class="col-md-3 four-grid">
    <div class="four-grid2">
      <div class="icon">
        <i class="glyphicon glyphicon-briefcase" aria-hidden="true"></i>
      </div>
      <div class="four-text">
        <h3>Mochilas</h3>
        <h4>2</h4>
      </div>
      <a href="/ver-mochilas">Ver Mais</a>
    </div>
  </div>
  <div class="col-md-3 four-grid">
    <div class="four-grid3">
      <div class="icon">
        <i class="glyphicon glyphicon-book" aria-hidden="true"></i>
      </div>
      <div class="four-text">
        <h3>Matérias</h3>
        <h4>20</h4>
      </div>
      <a href="#" data-toggle="modal" data-target="#myModal1">Ver Mais</a>
    </div>
  </div>
  <div class="col-md-3 four-grid">
    <div class="four-grid4">
      <div class="icon">
        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
      </div>
      <div class="four-text">
        <h3>Anotações</h3>
        <h4>420</h4>
      </div>
      <a href="#" data-toggle="modal" data-target="#myModal1">Ver Mais</a>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<div class="grid-table <?php if(session('animation') == 1) {?> animated slideInLeft <?php } else { ?> animated bounceInDown <?php } ?>">
  <h3>Anotações recentes</h3>
  <select>
    <option value="">Selecione uma matéria</option>
    <option value=""></option>
    <option disabled>Mochila 1</option>
    <option>teste</option>
  </select>
  <div class="bottom-table">
    <div class="bs-docs-example">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Título</th>
            <th>Mochila</th>
            <th>Matéria</th>
            <th>Data</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Fila e Pilha - Estrutura de dados</td>
            <td>3º Semestre - Sistemas de Informação</td>
            <td>Algorítmo e Estrutura de Dados</td>
            <td>22/04/2017</td>
            <td>
              <a class="btn btn-primary btn-interact-table"><i class="fa fa-plus"></i></a>
              <a class="btn btn-danger btn-interact-table"><i class="fa fa-times"></i></a>
            </td>

          </tr>
          <tr>
            <td>Fila e Pilha - Estrutura de dados</td>
            <td>3º Semestre - Sistemas de Informação</td>
            <td>Algorítmo e Estrutura de Dados</td>
            <td>22/04/2017</td>
            <td>
              <a class="btn btn-primary btn-interact-table"><i class="fa fa-plus"></i></a>
              <a class="btn btn-danger btn-interact-table"><i class="fa fa-times"></i></a>
            </td>

          </tr>
          <tr>
            <td>Fila e Pilha - Estrutura de dados</td>
            <td>3º Semestre - Sistemas de Informação</td>
            <td>Algorítmo e Estrutura de Dados</td>
            <td>22/04/2017</td>
            <td>
              <a class="btn btn-primary btn-interact-table"><i class="fa fa-plus"></i></a>
              <a class="btn btn-danger btn-interact-table"><i class="fa fa-times"></i></a>
            </td>

          </tr>
          <tr>
            <td>Fila e Pilha - Estrutura de dados</td>
            <td>3º Semestre - Sistemas de Informação</td>
            <td>Algorítmo e Estrutura de Dados</td>
            <td>22/04/2017</td>
            <td>
              <a class="btn btn-primary btn-interact-table"><i class="fa fa-plus"></i></a>
              <a class="btn btn-danger btn-interact-table"><i class="fa fa-times"></i></a>
            </td>

          </tr>
          <tr>
            <td>Fila e Pilha - Estrutura de dados</td>
            <td>3º Semestre - Sistemas de Informação</td>
            <td>Algorítmo e Estrutura de Dados</td>
            <td>22/04/2017</td>
            <td>
              <a class="btn btn-primary btn-interact-table"><i class="fa fa-plus"></i></a>
              <a class="btn btn-danger btn-interact-table"><i class="fa fa-times"></i></a>
            </td>

          </tr>
          <tr>
            <td>Fila e Pilha - Estrutura de dados</td>
            <td>3º Semestre - Sistemas de Informação</td>
            <td>Algorítmo e Estrutura de Dados</td>
            <td>22/04/2017</td>
            <td>
              <a class="btn btn-primary btn-interact-table"><i class="fa fa-plus"></i></a>
              <a class="btn btn-danger btn-interact-table"><i class="fa fa-times"></i></a>
            </td>

          </tr>
          <tr>
            <td>Fila e Pilha - Estrutura de dados</td>
            <td>3º Semestre - Sistemas de Informação</td>
            <td>Algorítmo e Estrutura de Dados</td>
            <td>22/04/2017</td>
            <td>
              <a class="btn btn-primary btn-interact-table"><i class="fa fa-plus"></i></a>
              <a class="btn btn-danger btn-interact-table"><i class="fa fa-times"></i></a>
            </td>

          </tr>
          <tr>
            <td>Fila e Pilha - Estrutura de dados</td>
            <td>3º Semestre - Sistemas de Informação</td>
            <td>Algorítmo e Estrutura de Dados</td>
            <td>22/04/2017</td>
            <td>
              <a class="btn btn-primary btn-interact-table"><i class="fa fa-plus"></i></a>
              <a class="btn btn-danger btn-interact-table"><i class="fa fa-times"></i></a>
            </td>

          </tr>

        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection

@section('scripts_custom')

@endsection