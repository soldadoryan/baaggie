@extends('layout')

@section('css_custom')

<link href="/css/app.css" rel="stylesheet">
<link href="/css/home.css" rel="stylesheet">
<link href="/css/ver_mochilas.css" rel="stylesheet">

@endsection

@section('content')

<div class="painel fullheight animated bounceInDown">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/app">Página Inicial</a></li>
    <li class="breadcrumb-item active">Mochila</li>
    <li class="breadcrumb-item active">Ver Mochilas</li>
  </ol>
  <form class="form-search">
    <div class="row">
      <div class="col-lg-6">
        <div class="input-group">
          <input type="text" class="input-form" placeholder="Pesquisar por...">
          <button class="btn-sucesso"><i class="fa fa-search"></i></button>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="check-group text-right">
          <input type="checkbox" id="input_ativa" data-reverse checked data-group-cls="btn-group-md">
        </div>
      </div>
    </div>
  </form>
  <div class="row">
      <?php foreach($array_mochilas as $mochila) : ?>
      <div class="col-lg-4">
        <a href="">
          <div class="mochila">
            <img src="img/baaggie_md.png" alt="">
            <div class="desc">
              
              <label>{{ $mochila->titulo }}</label><br>
              <small><span><?php if($mochila->ativa == 1) { echo "<i class='fa fa-check'></i> Ativa"; } else { echo "Inativa"; } ?></span>
              <span><?php if($mochila->publica == 'P') { echo "<i class='fa fa-unlock'></i> Pública"; } else if($mochila->publica == 'L') { echo "<i class='fa fa-lock'></i> Privada"; } else { echo "<i class='fa fa-address-card-o'></i> Restrita"; } ?></small></span>
              <?php $date = new DateTime($mochila->created_at); ?><br>
              <small>Publicado em <?php echo $date->format('d/m/Y H:i'); ?></small>
            </div>
          </div>
        </a>
        </div>
    <?php endforeach; ?>
    <div class="col-lg-4">
        <a href="/cadastrar-mochila">
          <div class="mochila">
            <img src="img/baaggie_md.png" class="desativada" alt="">
            <div class="desc">
              
              <label>Cadastrar uma nova mochila!</label><br>
              <small>Clique aqui para você acessar o formulário de cadastro de mochilas.</small>
            </div>
          </div>
        </a>
        </div>
  </div>
</div>

@endsection

@section('scripts_custom')

<script src="js/app.js"></script>
<script src="js/check.js"></script>
<script src="js/ver_mochilas.js"></script>

@endsection