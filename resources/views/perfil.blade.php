@extends('layout')

@section('css_custom')

<link href="/css/app.css" rel="stylesheet">
<link href="/css/home.css" rel="stylesheet">

@endsection

@section('content')

<div class="painel  animated bounceInDown">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/app">Página Inicial</a></li>
    <li class="breadcrumb-item active">Meu Perfil</li>
  </ol>
  <form id="form_perfil" @submit.prevent="post_perfil">
    <div class="row">
      <div class="col-lg-6">
        <label>Nome Completo:</label>
        <input type="text" id="input_nome" class="input-form" value="{{ $user->nome }}">
      </div>
      <div class="col-lg-6">
        <label>Username:</label>
        <input type="text" class="input-form" value="{{ $user->username }}" disabled>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <label>Senha:</label>
        <input type="password" id="input_senha" class="input-form" value="{{ base64_decode($user->senha) }}">
      </div>
      <div class="col-lg-6">
        <label>Confirmar Senha:</label>
        <input type="password" id="input_confirmar_senha" class="input-form">
      </div>
    </div>
    <div class="row">
      <div class="col-lg-offset-8 col-lg-4">
        <div class="wrap-buttons">
          <button type="submit" class="btn-sucesso">Alterar Perfil</button>
          <button type="button" v-on:click="limpar_formulario" class="btn-atencao">Limpar Campos</button>
        </div>
      </div>
    </div>
  </form>
</div>

@endsection

@section('scripts_custom')

<script src="js/app.js"></script>
<script src="js/perfil.js"></script>

@endsection