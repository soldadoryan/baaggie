<html>
<head>
  <title>baaggie</title>
  <link rel="stylesheet" href="css/app.css ">
  <link rel="stylesheet" href="css/index.css ">
  <link rel="stylesheet" href="css/animate.css ">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="https://use.fontawesome.com/88f58b9255.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/animate.css@3.5.1" rel="stylesheet" type="text/css">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" type="image/png" href="fav.png">
</head>
<body>
  <!-- <div class="bar-top"></div> -->
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
        <div class="painel-login animated bounceInUp">
          <img src="/img/logob.png" class="logo img-responsive">
          <form id="form_login" @submit.prevent="post_login">
            <transition name="slide-fade">
              <div class="painel-erro" v-if="show"><i class="fa fa-times"></i> @{{ message }}</div>
            </transition>
            <label for="username">Usuário</label>
            <input type="text" id="input_username" name="input_username" required>
            <label for="username">Senha </label>
            <input type="password" id="input_senha" name="input_senha" required>
            <button type="submit" class="btn btn-warning"><i class="fa fa-arrow-right"></i></button>
          </form>
        </div>
      </div>
    </div>
  </div>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src="js/app.js"></script>
  <script src="js/index.js"></script>
</body>
</html>