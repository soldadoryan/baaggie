let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/index.js', 'public/js')
   .js('resources/assets/js/cadastrar_mochila.js', 'public/js')
   .js('resources/assets/js/ver_mochilas.js', 'public/js')
   .js('resources/assets/js/perfil.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/index.scss', 'public/css')
   .sass('resources/assets/sass/home.scss', 'public/css')
   .sass('resources/assets/sass/ver_mochilas.scss', 'public/css');
