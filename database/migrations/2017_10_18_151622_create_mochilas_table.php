<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMochilasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mochilas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->char('publica', 1);
            $table->boolean('ativa');
            $table->integer('id_autor')->unsigned();
            $table->foreign('id_autor')->references('id')->on('usuarios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mochilas');
    }
}
