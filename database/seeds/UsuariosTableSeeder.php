<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
        [

          'nome' => 'Ryan Drumond',
          'username' => 'ryandrumond',
          'senha' => base64_encode("123"),
          'created_at' => date("Y-m-d H:i:s"),

        ],
        [

          'nome' => 'Lucas Medeiros',
          'username' => 'lucasmedeiros',
          'senha' => base64_encode("123"),
          'created_at' => date("Y-m-d H:i:s"),

        ],
        ]);
    }
}
