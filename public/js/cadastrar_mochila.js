/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 43);
/******/ })
/************************************************************************/
/******/ ({

/***/ 43:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(44);


/***/ }),

/***/ 44:
/***/ (function(module, exports) {

var form_cadastrar_mochila = new Vue({
  el: "#form_cadastrar_mochila",
  methods: {
    post_cadastrar_mochila: function post_cadastrar_mochila() {

      var validacao = false;

      $("#form_cadastrar_mochila").find('input, select').each(function (i, v) {
        if ($(v).val() == '') {
          validacao = true;
          $(v).addClass("error");
        }
      });

      if (validacao == true) {
        swal_functions.swal_padrao('Campos vazios!', 'Por favor, preencha os campos com borda vermelha.', 'error', "Ok! Vou verificar.", false);
      } else {
        var dados = new FormData();

        dados.append("titulo", $("#input_titulo").val());
        dados.append("publica", $("#input_publica").val());
        var btn_check = $("#input_ativa").siblings('div').find('a.active');

        if (btn_check.attr('is') == 0) {
          dados.append("ativa", 1);
        } else {
          dados.append("ativa", 0);
        }

        axios.post("/cadastro-mochila", dados).then(function (response) {

          if (response.data == 1) {

            swal_functions.swal_padrao('Tudo certo!', 'Mochila criada com sucesso.', 'success', "Continuar", true, "/ver-mochilas");
          } else {

            swal_functions.swal_padrao('Erro do sistema!', response.data, 'error', "Ok! Tentarei mais tarde.", false);
          }
        }).catch(function (error) {

          swal_functions.swal_padrao('Erro do sistema!', error.message, 'error', "Ok! Tentarei mais tarde.", false);
        });
      }
    },
    limpar_formulario: function limpar_formulario() {

      $("#form_cadastrar_mochila input, #form_cadastrar_mochila select").removeClass("error");
      $("#input_nome").val("");
      $("#input_senha").val("");
      $("#input_titulo").val("");
      $("#blank_select").prop('selected', true);
      $("#input_ativa").prop('checked', true);
    }
  }
});

// checkbox js
$(':checkbox').checkboxpicker({
  html: true,
  offLabel: '<span class="glyphicon glyphicon-remove">',
  onLabel: '<span class="glyphicon glyphicon-ok">'
});

/***/ })

/******/ });