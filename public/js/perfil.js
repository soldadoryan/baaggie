/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 47);
/******/ })
/************************************************************************/
/******/ ({

/***/ 47:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(48);


/***/ }),

/***/ 48:
/***/ (function(module, exports) {

var form_perfil = new Vue({
  el: "#form_perfil",
  methods: {
    post_perfil: function post_perfil() {
      var validacao = false;

      $("#form_perfil").find('input').each(function (i, v) {
        if ($(v).val() == '') {
          validacao = true;
          $(v).addClass("error");
        }
      });

      if (validacao == true) {
        swal_functions.swal_padrao('Campos vazios!', 'Por favor, preencha os campos com borda vermelha.', 'error', "Ok! Vou verificar.", false);
      } else {
        if ($("#input_senha").val() == $("#input_confirmar_senha").val()) {

          var dados = new FormData();

          dados.append("nome", $("#input_nome").val());
          dados.append("senha", $("#input_senha").val());

          axios.post("/atualizar-perfil", dados).then(function (response) {

            if (response.data == 1) {

              swal_functions.swal_padrao('Tudo certo!', 'Perfil atualizado com sucesso.', 'success', "Continuar", false);

              $("#form_perfil input").removeClass("error");
              $("#input_confirmar_senha").val("");
            } else {

              swal_functions.swal_padrao('Erro do sistema!', response.data, 'error', "Ok! Tentarei mais tarde.", false);
            }
          }).catch(function (error) {

            swal_functions.swal_padrao('Erro do sistema!', error.message, 'error', "Ok! Tentarei mais tarde.", false);
          });
        } else {

          swal_functions.swal_padrao('Erro do sistema!', 'As senhas inseridas não são iguais.', 'error', "Ok! Vou verificar.", false);
        }
      }
    },
    limpar_formulario: function limpar_formulario() {

      $("#form_perfil input").removeClass("error");
      $("#input_nome").val("");
      $("#input_senha").val("");
      $("#input_confirmar_senha").val("");
    }
  }

});

/***/ })

/******/ });